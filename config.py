import os
# basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = os.environ.get('FLASK_SECRET')
    if not SECRET_KEY:
            raise Exception("Failed to load Secret Key")
    REDIS_URL = os.environ.get('REDIS_URL') or 'redis://'
    BEEMINDER_DATABASE = "beeminder.db"
    USERS_DATABASE = "users.db"
    POSTS_DATABASE = "posts.db"
    MDBG_FILE = "cedict_1_0_ts_utf-8_mdbg.txt"
