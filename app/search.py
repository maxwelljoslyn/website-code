from app import app
from flask import request, redirect, url_for, send_file, send_from_directory, render_template, make_response, abort, jsonify, escape
from urllib.parse import urlparse
from pathlib import Path
import datetime as dt
from .utility import mp_request_into_dict, write_post, read_post, _load_markdown
import os, glob, pprint, pytz, sqlite3, requests, re

@app.route("/api/v1/search/mdbg", methods=["GET", "POST"])
def search_mdbg():
    if request.method == "POST":
        return "Not yet implemented", 501
    else:
        data = None
        if request.json:
            data = request.get_json()
        else:
            data = request.values
        if data == None:
            return "Bad request", 400
        else:
            query = data.get("query")
            if query is None:
                return "Bad request: no 'query' GET parameter", 400
            else:
                shamwow = f"<p><strong>You bet I got that GET!</strong></p><p>Your query was:</p><p>{escape(query)}...</p>"
                billion = _search_in_mdbg(query)
                fooligan = f"<p>...but I'm just gonna ignore it. Here are ~70 lines from CC-CEDICT instead: {billion}</p>"
                return shamwow+fooligan, 200

def _search_in_mdbg(query):
    text = None
    with open(app.config["MDBG_FILE"], "r") as fp:
        text = fp.read()
        text = text.split("\n")
    # fixme this regex is incorrect for getting rid of "# " lines
    pattern = re.compile("^#\w+$")
    text = [t for t in text if not re.search(pattern, t)]
    return text[25:100]
              
    
