from app import app, login_manager
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login.mixins import UserMixin
import sqlite3
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')


class User(UserMixin):

    def __init__(self, username, password_hash=None):
        self.username = username
        if password_hash:
            self.password_hash = password_hash
    
    def get_id(self):
        return self.username

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    
@login_manager.user_loader
def load_user(username):
    conn = sqlite3.connect(app.config["USERS_DATABASE"])
    conn.row_factory = sqlite3.Row
    try:
        c = conn.cursor()
        c.execute("SELECT username, password_hash FROM users WHERE username = :username", {"username" : username})
        result = c.fetchone()
        c.close()
        conn.close()
        if result:
            return User(result["username"], result["password_hash"])
        else:
            return None
    except sqlite3.Error as e:
        print("error: ", e)
        raise e
