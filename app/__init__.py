from flask import Flask
from config import Config
from redis import Redis
from flask_login import LoginManager
import rq

app = Flask(__name__, static_folder="material/static")
app.config.from_object(Config)
# add Beeminder settings
app.config.from_json("bee_settings.json")

app.redis = Redis.from_url(app.config['REDIS_URL'])
app.task_queue = rq.Queue('website-tasks', connection=app.redis)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# app.login_manager = LoginManager(app)
# app.login_manager.init_app(app)

# always do at the end: any files containing routes
from app import routes, bee_routes, search
