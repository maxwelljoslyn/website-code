from app import app
from flask import jsonify, render_template, request
from flask_login import current_user
from pathlib import Path
from urllib.parse import urlparse
import datetime as dt
import requests as r
import json, re, mistune, os, jinja2, pytz, sqlite3

DOMAIN = "www.maxwelljoslyn.com"

MASTERS_FOLDER = os.path.join(app.root_path, "masters")
MATERIAL_FOLDER = os.path.join(app.root_path, "material")
PAGES_FOLDER = os.path.join(MATERIAL_FOLDER, "pages")
POEMS_FOLDER = os.path.join(MATERIAL_FOLDER, "poems")

# generated files live in one folder for ease of .gitignore
GENERATED_FILES_FOLDER = os.path.join(MATERIAL_FOLDER, "generated")
if not Path(GENERATED_FILES_FOLDER).exists():
    Path(GENERATED_FILES_FOLDER).mkdir()
BLOG_FILE = os.path.join(GENERATED_FILES_FOLDER, "blog")
POEMSINDEX_FILE = os.path.join(GENERATED_FILES_FOLDER, "poemsindex")
ALLTAGS_FILE = os.path.join(GENERATED_FILES_FOLDER, "alltags")
TAGS_FOLDER = os.path.join(GENERATED_FILES_FOLDER, "tags/")
if not Path(TAGS_FOLDER).exists():
    Path(TAGS_FOLDER).mkdir()
    
def make_post():
    # fixme: for retroactive posting (published time is already provided, and is in the past), will need to change y/m/d path creation to respect the published prop.
    # fixme: allow future published time?
    creation_time = dt.datetime.now(pytz.timezone('US/Pacific'))
    year = f"{creation_time.year:02}"
    month = f"{creation_time.month:02}"
    day = f"{creation_time.day:02}"
    datepath = Path(year, month, day)
    storagedir = Path(MATERIAL_FOLDER) / datepath
    if not storagedir.exists():
        storagedir.mkdir(parents=True)
    payload = mp_request_into_dict(creation_time.strftime("%Y-%m-%dT%H:%M:%S%z"))
    slug = Path(str(new_post_identifier(payload, storagedir)))
    # to minimize chance of race condition, touch file, so other micropub posts 
    # fixme: am I doing something stupid here?
    # answer: there's probably a smarter way
    storagepath = storagedir / slug
    storagepath.touch()
    permalink = "https://" + str(DOMAIN / datepath / slug)
    payload["properties"]["url"] = permalink
    write_post(storagepath, payload)
    return permalink, payload

def mp_request_into_dict(creation_time):
    if request.is_json:
        post = request.get_json()
        if "access_token" in post["properties"]:
            del post["properties"]["access_token"]
    else:
        props = dict(request.form)
        h = props.pop("h")
        if "access_token" in props:
            del props["access_token"]
        # spec defines "category" key as only supporting a single tag
        # some clients defy spec and send multiple categories under "category"
        #  thus, use getlist() instead of get()
        props["category"] = request.form.getlist("category")
        if props["category"]:
            if not isinstance(props["category"], list):
                props["category"] = list(props["category"])
        else:
            props["category"] = request.form.getlist("category[]")
            if "category[]" in props:
                del props["category[]"]
        post = {"h" : h, "properties" : props}
    post["properties"]["published"] = [creation_time]
    # if post["properties"].get("visibility") == "private": 
   #     if post["properties"].get("audience"):
    #         # make sure I'm included
    #         post["properties"]["audience"].append("maxwell")
    #     else:
    #         # for my eyes only
    #         post["properties"]["audience"] = ["maxwell"]
    return post

def write_post(where_to_store, post):
    with open(where_to_store, "w") as f:
        json.dump(post, f)

def _load_markdown(source):
    # escape=False prevents Markdown from escaping HTML mixed into the Markdown source
    # this is desirable because I use a lot of HTML to fill in Markdown's gaps
    markdownifier = mistune.create_markdown(escape=False,
                                            renderer="html", # this is NOT the same as instantiating mistune.HTMLRenderer and passing it in place of "html"
                                            plugins=["url", "footnotes"])
    properties = {}
    hyphen_lines = 0
    bodylist = []
    reached_body = False
    try:
        fp = open(source, "r")
        for line in fp:
            if line == "---\n" and hyphen_lines > 0:
                # we've reached the end of the properties
                reached_body = True
                continue
            elif line == "---\n":
                hyphen_lines += 1
            elif not reached_body:
                spot = line.index(":")
                key = line[:spot] # YAML keyword
                val = line[spot+1:] # YAML value
                val = val.strip()
                properties[key] = val
                continue
            else:
                # todo: log a warning if line contains "(www" since that's probably a link which needs http(s):// protocol, lest HTML renderer, by default, emit a link relative to maxwelljoslyn.com
                bodylist.append(line)
    except FileNotFoundError as e:
        raise e

    # per MF2 JSON standard, store HTML content as value of "html" key in dict which is an element of an array (Python list)
    content = markdownifier("".join(bodylist))
    q = dict(html=content)
    d = [q]
    properties["content"] = d

    # rename Markdown "title" property to MF2 JSON "name"
    if "title" in properties:
        properties["name"] = properties["title"]

    
    if "time" not in properties:
        # all the early posts lacking a time entry were made during Pacific Daylight Time
        # 12:34:56 is my typical placeholder timestamp
        properties["time"] = "12:34:56-0700"

    # parse date and time properties into publication attribute
    pubdate = properties.get("date")
    pubtime = properties.get("time")
    if pubdate and pubtime:
        # 1-element list to conform with microformats JSON syntax
        properties["published"] = [pubdate + "T" + pubtime]

    if "update-date" and "update-time" in properties:
        # 1-element list to conform with microformats JSON syntax
        properties["updated"] = [properties["update-date"] + "T" + properties["update-time"]]

    if "tags" in properties:
        # rename to MF2 JSON "category" (singular even though it is always a list)
        # also, anything using "tags" has tags stored as a comma-separated string, e.g. "cats, dogs"
        # that needs to be a list ["cats", "dogs"]
        properties["category"] = properties["tags"].split(", ")
        del properties["tags"]
        
    result = dict(type="entry",properties=properties)
    return result
        
def read_post(source):
    """Papers over differences between Markdown and JSON storage while retrieving posts from disk. Ultimately this function will instead query the database."""
    try:
        post = _load_markdown(source+".md")
    except FileNotFoundError:        
        with open(source, "r") as fp:
            try:
                post = json.load(fp)
            except:
                raise Exception(f"Couldn't load JSON source: {source}")

    # todo: sanitization, "only one way" to have post.published, post.updated, etc. - take all that logic out of templates and do it here
    pubdate = post["properties"].get("date")
    pubtime = post["properties"].get("time")
    fullpub = post["properties"].get("published")
    if fullpub:
        pass
    elif pubdate and pubtime:
        post["properties"]["published"] = [pubdate + "T" + pubtime]
    elif pubdate:
        post["properties"]["published"] = [pubdate + "T12:34:56-0700"]
    elif pubtime:
        post["properties"]["published"] = ["1990-12-31" + "T" + pubtime]
    else:
        post["properties"]["published"] = ["1990-12-31TT12:34:56-0700"]
    if fullpub and not isinstance(fullpub, str):
        post["properties"]["published"] = fullpub[0]
    p2 = post["properties"].get("name", "a note")
    if not isinstance(p2, str):
        post["properties"]["name"] = p2[0]
    
    # set non-microformats properties, only used internally for templating
    if "wm" not in post["properties"]:
        post["properties"]["wm"] = True
    if "show-metadata" not in post["properties"]:
        post["properties"]["show-metadata"] = True
    return post


class MicropubException(Exception):
    pass

class NotAuthorized(MicropubException):
    """Client failed to provide an authorization token."""
    def __init__(self, message):
        self.message = message

class NotAuthenticated(MicropubException):
    """Client could not be authenticated."""

    def __init__(self, message):
        self.message = message

def authorize():
    # determine if valid authorization token is supplied
    token = request.headers.get("authorization")
    verification_headers = {}
    if token:
        verification_headers["authorization"] = token
    else:
        print("Token not supplied in Authorization header. Looking for token in form parameter.")
        token = request.form.get("access_token")
        if token:
            verification_headers["authorization"] = token
        else:
            raise NotAuthorized("Token not supplied in Authorization header, nor in access_token form field. Thus, you are not authorized.")
    # token supplied. use it to verify
    # fixme: hardcoded token_endpoint could be discovered with HEAD request to my domain
    token_endpoint = "https://tokens.indieauth.com/token"
    verification = r.get(token_endpoint, headers=verification_headers)
    if "error" in verification.headers:
        raise NotAuthenticated("Token endpoint declares your token invalid.")
    else:
        return



def _load_all():
    page_paths = list(Path(PAGES_FOLDER).glob("*"))
    poem_paths = list(Path(POEMS_FOLDER).glob("*"))
    ymd_paths = list(Path(MATERIAL_FOLDER).glob("20*/**/*"))
    pages = [read_post(str(x.with_suffix(""))) for x in page_paths]
    poems = [read_post(str(x.with_suffix(""))) for x in poem_paths]
    ymds = [read_post(str(x.with_suffix(""))) for x in ymd_paths if not x.is_dir()]
    pages.extend(poems)
    pages.extend(ymds)
    return pages


def _should_include_in_blog(post):
    path = urlparse(post["properties"]["url"]).path
    if path[1:] in ("", "index", "blog", "testpage", "test"):
        return False
    elif post["properties"].get("visibility") == "private":
        return False        
    else:
        return True

def publication(post):
    "Return post's 'published' property, abstracting away from the fact that some posts store(d) this as a string while others have it in a one-element list (per MF2 JSON syntax)."
    # fixme: create a Post class which abstracts away from the 'single element list' awkwardness for all JSON properties
    # this function would then become one of that class's methods
    # and many of the properties.get() calls throughout this codebase would become calls to Post methods
    pub_prop = post["properties"].get("published")
    # fixme - fails if post lacks 'published' date, since .get() would return None and that can't be sorted with the other publish times
    if isinstance(pub_prop, str):
        return pub_prop
    else:
        return pub_prop[0]

TAG_DESCRIPTIONS = {"poetry" : "<strong>Looking to read poems?</strong> Visit <a href='/poems'>the poems index</a>.",
                    "bna" : "\"BNA\" stands for \"before and after,\" indicating a comparison of inputs and outputs, or rough drafts and rewrites.",
                    "translation" : "<strong>Looking to read translations?</strong> Visit <a href='/translation'>the translation index</a>."}

def generate_some_tagpages(which=None):
    pass

def next_prev(permalink):
    conn = sqlite3.connect(app.config["POSTS_DATABASE"])
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute("""select * from posts
    where published > (select published from posts where permalink = ?)
    order by published
    limit 1""", (permalink,))
    # wrap permalink in tuple because otherwise sqlite treats it as an iterable, i.e. 30+ individual character values -__-
    # "strings are iterable by default" strikes again
    next = c.fetchone()
    c.execute("""select * from posts
    where published < (select published from posts where permalink = ?)
    order by published desc
    limit 1""", (permalink,))
    prev = c.fetchone()
    c.close()
    conn.close()
    if next:
        # Row object supports bracket access but not get() - didn't know that was possible, but you try it
        next = next["permalink"]
    if prev:
        prev = prev["permalink"]
    return {"next" : next, "prev" : prev}

# fixme: pass next_prev into page-rendering template, e.g.
# return render_template("base.html", next_prev=utility.next_prev(props.permalink))

def generate_all_tagpages():
    all_posts = _load_all()
    all_tags = {}
    for p in all_posts:
        if p["properties"].get("visibility") == "private":
            continue
        else:
            cs = p["properties"].get("category")
            # has tags?
            if cs:
                for c in cs:
                    if c not in all_tags:
                        all_tags[c] = [p]
                    else:
                        all_tags[c].append(p)
    jinja_env = jinja2.Environment(autoescape=False,
                                   loader=jinja2.FileSystemLoader(os.path.join(app.root_path, "templates")))
    one_tag_template = jinja_env.get_template("one_tag_body.html")
    for tag in all_tags:
        all_tags[tag].sort(key=lambda p: publication(p), reverse=True)
        posts = all_tags[tag]
        the_page = one_tag_template.render(tag=tag,
                                           desc=TAG_DESCRIPTIONS.get(tag),
                                           posts=posts,
                                           num=len(posts))
        actual_file = TAGS_FOLDER + tag
        temp_file = actual_file + "temp"
        with open(temp_file, "w") as fp:
            fp.write(the_page)
        os.replace(temp_file, actual_file)

    alltags_template = jinja_env.get_template("alltags_body.html")
    with open(ALLTAGS_FILE, "w") as fp:
        # fixme: add a per-tag count of posts
        the_page = alltags_template.render(tags=sorted(list(all_tags)))
        actual_file = ALLTAGS_FILE
        temp_file = actual_file + "temp"
        with open(temp_file, "w") as fp:
            fp.write(the_page)
        os.replace(temp_file, actual_file)
        
        
def generate_blog_body():
    all_posts = _load_all()
    blogworthies = []
    conn = sqlite3.connect(app.config["POSTS_DATABASE"])
    conn.row_factory = sqlite3.Row
    c = conn.cursor()
    c.execute("DELETE FROM posts")
    for post in all_posts:
        if not _should_include_in_blog(post):
            continue
        props = post["properties"]
        data = {"permalink" : props["url"],
                "published" : props["published"],
                "name" : props.get("name")}
        c.execute("INSERT INTO posts (permalink, published, name) VALUES (:permalink, :published, :name)", data)
        blogworthies.append(post)
    conn.commit()
    c.close()
    conn.close()
    blogworthies.sort(key=lambda p:p["properties"].get("published"), reverse=True)
    jinja_env = jinja2.Environment(autoescape=False,
                                   loader=jinja2.FileSystemLoader(os.path.join(app.root_path, "templates")))
    template = jinja_env.get_template("blog_body.html")
    blog_body = template.render(entries=blogworthies, num_posts=len(blogworthies))
    blog_body_temporary_loc = BLOG_FILE + "temp"
    blog_body_normal_loc = BLOG_FILE
    with open(blog_body_temporary_loc, "w") as fp:
        fp.write(blog_body)
    os.replace(blog_body_temporary_loc, blog_body_normal_loc)
    # del all_posts
    return








def _load_poems():
    poem_paths = list(Path(POEMS_FOLDER).glob("*"))
    poems = {path.with_suffix("") : read_post(str(path.with_suffix(""))) for path in poem_paths}
    return poems

def generate_poemsindex_body():
    all_poems = _load_poems()
    # "Hooked on Darjeeling" and earlier have a fixed non-chronological order
    # poems since "Hooked" are displayed in chrono order
    # poems_upto_hooked = [Path(POEMS_FOLDER) / post for post in \
    #                      ["hooked-on-darjeeling",
    #                       "night-walk",
    #                       "thats-podracing",
    #                       "esther",
    #                       "missing-the-moment",
    #                       "milkshake",
    #                       "fv",
    #                       "bunghole",
    #                       "gift-gadi",
    #                       "peerless-drongo",
    #                       "drongo-explained",
    #                       "hayley-boytoy",
    #                       "hayley-goodbye",
    #                       "ridge",
    #                       "laughodils",
    #                       "chance",
    #                       "cameron-felix",
    #                       "after-farewell-lunch"]]
    after_hooked = []
    hooked_and_before = []
    hooked_pub = publication(all_poems[Path(POEMS_FOLDER) / "hooked-on-darjeeling"])
    for post in all_poems.values():
        pub = publication(post)
        if pub > hooked_pub:
            after_hooked.append(post)
        else:
            hooked_and_before.append(post)
    after_hooked.sort(key=lambda p: publication(p), reverse=True)
    jinja_env = jinja2.Environment(autoescape=False,
                                   loader=jinja2.FileSystemLoader(os.path.join(app.root_path, "templates")))
    template = jinja_env.get_template("poemsindex_body.html")
    poemsindex_body = template.render(after_hooked=after_hooked,
                                      hooked_and_before=hooked_and_before)
    poemsindex_body_temporary_loc = POEMSINDEX_FILE + "temp"
    poemsindex_body_normal_loc = POEMSINDEX_FILE
    with open(poemsindex_body_temporary_loc, "w") as fp:
        fp.write(poemsindex_body)
    os.replace(poemsindex_body_temporary_loc, poemsindex_body_normal_loc)
    return

def int_or_nothing(a_string):
    try:
        return int(a_string)
    except ValueError:
        return None

def new_post_identifier(post, folder):
    if "mp-slug" in post["properties"]:
        return post["properties"].get("mp-slug")
    # all files in folder, without extensions so I only consider filenames
    posts_that_day = [os.path.splitext(post)[0] for post in os.listdir(folder)]
    if not posts_that_day:
        # first post of the day
        return 1
    else:
        # remove posts with non-numeric identifiers
        posts_that_day = [int_or_nothing(post) for post in posts_that_day]
        posts_that_day = [q for q in posts_that_day if q != None]
        # may now be empty
        if not posts_that_day:
            return 1
        else:
            return (1 + max(posts_that_day))



def allowed_to_view(post):
    vis = post["properties"].get("visibility")
    audience = post["properties"].get("audience")
    if vis != "private":
        # happy path for most common case
        return True
    if not audience:
        # if vis is private, there SHOULD be an audience, so this stems from some error on my part - but let's fail safe!
        return False
    if not current_user.is_authenticated:
        # don't betray existence
        return False
    elif current_user.is_authenticated and current_user.username not in audience:
        # not allowed!
        return False
    else:
        # allowed - authenticated AND in audience
        return True


# let ncs be the list of items to be displayed in non chronological order, where the LAST item is the one to be displayed at the bottom of the page, i.e. farewell lunch
# get everything in allpoems that's NOT in NCS to find the things which are "since darjeeling"
# for each of those, write them to index_body
# then, # for item in ncs:
# write item to poemsindex_body
# delete its entry from allpoems



# just pop the non-chrono items out of the dictionary  of the dictionary then insert everything remainig in chrono at the 


    


# recklessly calling functions at import time
# all this blog stuff is one giant code smell anyway which is pretty pathetic
# the real answer is "database" but I'm not ready FFS



generate_blog_body()
generate_poemsindex_body()
generate_all_tagpages()


# don't uncomment these unless you want to go to hell

# blog_job = app.task_queue.enqueue("app.utility.generate_blog_body")
# or just
# blog_job = app.task_queue.enqueue(generate_blog_body)

# fixme - why, if I background these jobs, do they start going ALL THE TIME? this is a perfect Angelo question. and it is my punishment for using rq/redis without fully understanding it

# wild guess: is utility.py being imported ... over and over, possibly once on each GET? WTF?

#  no, that can't be it. if this file WERE getting imported over and over, then calling generate_* at the top level, as done here, would make every request lock up as the funs executed

# WAIT YEAH THAT IS IT!!!!!
# I WAS CALLING RQ WITH THE FULLY QUALIFIED 'APP.UTILITY.FUNCTION_NAME' WHICH MEANT IT ___WAS___ IMPORTING UTILITY OVER AND OVER!!
# I'M A REAL PROGRAMMER JUST LIKE PINOCCHIO

# UPDATE: I have returned to being a wooden puppet
# I continue to not grok RQ which is my fault
# even doing them in the not fully qualified form still resulted in INFINITE IMPORT LOOP OF RQ JOBS

# and I got the same problem if I put them into __init__, too.
# how do I get them to just run once at app startup time? what part of the puzzle am I missing?

# > rq empty website-tasks
# 43 jobs removed from website-tasks queue
# ho li fuk!
