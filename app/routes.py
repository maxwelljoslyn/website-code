from app import app
from flask import request, redirect, url_for, send_file, send_from_directory, render_template, make_response, abort, flash, jsonify
from urllib.parse import urlparse
from pathlib import Path
from .utility import read_post, DOMAIN, MATERIAL_FOLDER, PAGES_FOLDER, POEMS_FOLDER, BLOG_FILE, POEMSINDEX_FILE, ALLTAGS_FILE, TAGS_FOLDER, NotAuthorized, NotAuthenticated, authorize, make_post, MASTERS_FOLDER, _load_markdown, allowed_to_view, next_prev, INTERVIEWS_FOLDER
from flask_login import current_user, login_user, logout_user, login_required
from .login import load_user, LoginForm
import os, glob, pprint, enum

@app.route("/page-bodies.txt")
def old_page_bodies():
    with open(os.path.join(MATERIAL_FOLDER, "page-bodies.txt"), "r") as fp:
        pagebodies = fp.read()
    return pagebodies


@app.route("/blog")
def blog():
    blog_body = None
    with open(BLOG_FILE, "r") as fp:
        blog_body = fp.read()
    return render_template("blog.html", blog_body=blog_body)

@app.route("/index")
def index():
    filepath = os.path.join(PAGES_FOLDER, "index")
    indexpage = read_post(filepath)
    return render_template("base.html", post=indexpage)

@app.route("/")
@app.route("/homepage")
def redirect_homepage():
    return redirect(url_for("index"))

# fixme: ensure a_page() doesn't interfere with routes like "/2020" when those are created
# fixme: ensure a_page() doesn't interfere with "/poems" (or "/poems/" if I have to resort to that)

@app.route("/static/fedoradventure/play")
def old_play_fedoradventure():
    return redirect(url_for("play_fedoradventure"))

@app.route("/fedoradventure/play")
def play_fedoradventure():
    zongo = os.path.join(MATERIAL_FOLDER, "static/fedoradventure/play.html")
    with open(zongo, "r") as f:
        return f.read()


@app.route("/fedoradventure/download")
def download_fedoradventure():
    return send_from_directory(app.static_folder,
                               "fedoradventure/Fedoradventure.gblorb",
                               as_attachment=True)
    
@app.route("/static/fedoradventure/Fedoradventure.gblorb")
def redirect_download_fedoradventure():
    return redirect(url_for("download_fedoradventure"))


@app.route("/static/ryanquest/play")
def old_play_ryanquest():
    return redirect(url_for("play_ryanquest"))

@app.route("/ryanquest/play")
def play_ryanquest():
    zongo = os.path.join(MATERIAL_FOLDER, "static/ryanquest/play.html")
    with open(zongo, "r") as f:
        return f.read()


@app.route("/ryanquest/download")
def download_ryanquest():
    return send_from_directory(app.static_folder,
                               "ryanquest/RYAN QUEST.gblorb",
                               as_attachment=True)
    
@app.route("/static/ryanquest/RYAN-QUEST.gblorb")
def redirect_download_ryanquest():
    return redirect(url_for("download_ryanquest"))


@app.route("/<page>")
def a_page(page):
    "Route to top-level pages like About, Sith Lord Challenge. These are mostly remnants of the original static-site permalink scheme, where all articles got top-level paths."
    # add: if int(page) >= 2018: sent to that archive page; else, redirect to index and flash "this site began in 2018, you've asked for 2017, nada nada!"
    filepath = os.path.join(PAGES_FOLDER, page)
    try:
        data = read_post(filepath)
    except:
        abort(404)
    if allowed_to_view(data):
        return render_template("base.html", post=data, np=next_prev(request.url))
    else:
        abort(404)
        
@app.route("/<year>/<month>/<day>/<slug>")
def a_post(year, month, day, slug):
    filepath = os.path.join(MATERIAL_FOLDER, year, month, day, slug)
    try:
        page = read_post(filepath)
    except:
        # fixme "except FileNotFound" then have read_post (re) raise that error, if it doesn't already
        abort(404)
    if allowed_to_view(page):
        return render_template("base.html", post=page, np=next_prev(request.url))
    else:
        abort(404)

# fixme: if archive pages are established at e.g. DOMAIN/2020/04, change redirect_old_notes and redirect_old_blog to fill in what values it can, and set empty strings for the rest (so that e.g. /route/y/m can redirect to /y/m without day and slug also needing to be filled)
@app.route("/notes/<year>/<month>/<day>/<slug>")
def redirect_old_notes(year, month, day, slug):
    return redirect(url_for("a_post", year=year, month=month, day=day, slug=slug))

@app.route("/blog/<year>/<month>/<day>/<slug>")
def redirect_old_blog(year, month, day, slug):
    return redirect(url_for("a_post", year=year, month=month, day=day, slug=slug))

@app.route("/blog/")
def redirect_old_blog_trailing():
    return redirect(url_for("blog"))

@app.route("/tags")
def tags():
    with open(ALLTAGS_FILE, "r") as fp:
        alltags_body = fp.read()
    return render_template("alltags.html", alltags_body=alltags_body)

@app.route("/alltags")
@app.route("/all-tags")
@app.route("/all_tags")
@app.route("/tag")
def redirect_tags():
    return redirect(url_for("tags"))

@app.route("/tags/", defaults={"a_tag" : ""})
@app.route("/tags/<a_tag>")
def a_tag(a_tag):
    if not a_tag:
        return redirect(url_for("tags"))
    with open(TAGS_FOLDER + a_tag, "r") as fp:
        tag_body = fp.read()
    return render_template("one_tag.html", a_tag=a_tag, tag_body=tag_body)

@app.route("/tag/", defaults={"a_tag" : ""})
@app.route("/tag/<a_tag>")
def redirect_a_tag(a_tag):
    return redirect(url_for("a_tag", a_tag=a_tag)) # this function handles empty a_tag value on its own

# helper and language redirects
# these will eventually go into their own dict, or an overall dict of "erroneous routes", so I don't have to write separate functions for them
@app.route("/makuxiansheng") # mistake
@app.route("/mrbreeches") # lang
@app.route("/mr-breeches") # lang
@app.route("/misterbreeches") # lang
@app.route("/mister-breeches") # lang
def redirect_mr_breeches():
    return redirect(url_for("a_page", page="maku-xiansheng"))

@app.route("/tieniuhebingya") # mistake
@app.route("/tieniuhe-bingya") # mistake
@app.route("/tieniu-hebingya") # mistake
@app.route("/tie-niu-he-bing-ya") # mistake
@app.route("/ironbull") # lang
@app.route("/iron-bull") # lang
def redirect_tieniu():
    return redirect(url_for("a_page", page="tieniu-he-bingya"))

@app.route("/poems/goodbye-hayley") # really this should be the canonical URL. classic case of letting an implementation detail (the storage path) leak into the public interface (the url). ah well, in the future I'll change the URL
def redirect_goodbye_hayley():
    return redirect(url_for("a_poem", poemname="hayley-goodbye"))

@app.route("/poems")
def poems():
    with open(POEMSINDEX_FILE, "r") as fp:
        poemsindex_body = fp.read()
    return render_template("poemsindex.html", poemsindex_body=poemsindex_body)

@app.route("/poetry")
@app.route("/poetry/index")
def redirect_poetry():
    return redirect(url_for("poems"))
    

@app.route("/poems/index")
def redirect_old_poemsindex():
    return redirect(url_for("poems"))

@app.route("/poems/", defaults={"poemname" : ""})
@app.route("/poems/<poemname>")
def a_poem(poemname):
    # todo upgrade - if poemname is bad, and would otherwise return 404, flash "not found but I'll search for ya!", redirect to /poems and do a search in JS ... is that stupid, or just dumb?
    if poemname == "":
        return redirect(url_for("poems"))
    filepath = os.path.join(POEMS_FOLDER, poemname)
    try:
        page = read_post(filepath)
    except:
        abort(404)
    if allowed_to_view(page):
        return render_template("base.html", post=page, np=next_prev(request.url))
    else:
        abort(404)

@app.route("/thedrongo/subscribe")
def redirect_subscribe():
    return redirect("https://newsletter.maxwelljoslyn.com/subscribe")
        

@app.route("/interviews/", defaults={"interviewname" : ""}) # top level
@app.route("/interviews/<interviewname>")
@app.route("/interview/", defaults={"interviewname" : ""}) # top level, singular
@app.route("/interview/<interviewname>")
@app.route("/thedrongo/interview/", defaults={"interviewname" : ""}) # under /thedrongo, singular
@app.route("/thedrongo/interview/<interviewname>")
def redirect_interviews(interviewname):
    return redirect(url_for("an_interview", interviewname=interviewname))


# xxx fixme why does this cause infinite redirect? Gahhhhh. I just want to make sure that /thedrongo/ trailing slash redirects to /thedrongo. it works fine for /poems/ above,
#but maybe some shit is going down and 'interviews' is a dummy path segment anyway ... gah, too kludgy for my own good
#@app.route("/thedrongo/")
#def redirect_drongo_trailing():
    #return redirect("https://www.maxwelljoslyn.com/thedrongo")

# this also didnt work :( with and without 'interviewname' as path parameter
#@app.route("/thedrongo/", defaults={"interviewname" : ""})
#def redirect_drongo_trailing(interviewname):
    #return redirect("https://www.maxwelljoslyn.com/thedrongo")

@app.route("/thedrongo/interviews/", defaults={"interviewname" : ""})
@app.route("/thedrongo/interviews/<interviewname>")
def an_interview(interviewname):
    if interviewname == "":
        return redirect("https://www.maxwelljoslyn.com/thedrongo")
    filepath = os.path.join(INTERVIEWS_FOLDER, interviewname)
    try:
        page = read_post(filepath)
    except:
        abort(404)
    if allowed_to_view(page):
        return render_template("base.html", post=page, np=next_prev(request.url))
    else:
        abort(404)


@app.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("index"))
    form = LoginForm()
    if form.validate_on_submit():
        if not form.username.data:
            flash("Invalid username")
            # fixme : preserve any existing "next" argument
            return redirect(url_for("login"))
        user = load_user(form.username.data)
        if not user:
            flash("Invalid username")
            # fixme : preserve any existing "next" argument
            return redirect(url_for("login"))
        if not user.check_password(form.password.data):
            flash("Invalid password")
            # fixme : preserve any existing "next" argument
            return redirect(url_for("login"))
        login_user(user, remember=form.remember_me.data)
        next_page = request.values.get("next")
        if next_page:
            # fixme: ensure that 'next' is on my domain
            # merely prefixing next_page with DOMAIN wasn't enough
            return redirect(next_page)
        else:
            return redirect(url_for("index"))
    return render_template("login.html", form=form)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("index"))

@app.route("/micropub", methods=['GET', 'POST'])
def micropub():
    try:
        authorize()
    except NotAuthorized as e:
        return jsonify({"error" : "forbidden",
                        "error_description" : "Sorry, you're not authorized"},
                       403)
    except NotAuthenticated as e:
        return jsonify({"error" : "unauthorized",
                        "error_description" : "Sorry, your access token couldn't be authenticated."},
                       401)
    if request.method == "POST":
        # fixme: switch on mp server command property to see whether this is create/delete/update
        permalink, payload = make_post()
        # start background jobs to regenerate pages
        # fixme: if post has any visibility other than public, no need to regenerate anything
        blog_job = app.task_queue.enqueue("app.utility.generate_blog_body")
        if payload["properties"].get("category"):
            tags_job = app.task_queue.enqueue("app.utility.generate_all_tagpages")
        
        response = make_response("Successfully created a payload!", 201)
        response.headers["Location"] = urlparse(permalink).path
        return response, 201
    else:
        # GET
        targets = { "uid": "https://myfave.example.org/mj",
                    "name": "Maxwell Joslyn on My Fave",
                    "service": {
                        "name": "My Fave",
                        "url": "https://myfave.example/",
                        "photo": "https://myfave.example/img/icon.png"},
                    "user": {
                        "name": "mj",
                        "url": "https://my.example/mj",
                        "photo": "https://myfave.example/mj/photo.jpg"}}
        configuration = {"syndicate-to" : [targets],
                         "visibility" : ["public", "private"],
                         "media-endpoint" : "https://www.maxwelljoslyn.com/micropub-media"}
        query = request.args.get("q")
        if query == "config":
            return configuration, 200
        elif query == "syndicate-to":
            return {"syndicate-to" : [targets]}, 200
        elif query == "source":
            # fixme I haven't tested this source query
            permalink = request.args.get("url")
            storage_path = Path(MATERIAL_FOLDER + urlparse(permalink).path)
            data = read_post(str(storage_path))
            return data, 200
        else:
            #fixme handle other queries, server commands
            return "That query is not implemented", 501


@app.route("/micropub-media", methods=["GET", "POST"])
def micropub_media():
    try:
        authorize()
    except NotAuthorized as e:
        return jsonify({"error" : "forbidden",
                        "error_description" : "Sorry, you're not authorized"},
                       403)
    except NotAuthenticated as e:
        return jsonify({"error" : "unauthorized",
                        "error_description" : "Sorry, your access token couldn't be authenticated."},
                       401)
    if request.method == "POST":
        dummy_path = url_for("static", filename="dummy.png")
        f = request.files
        q = request.values
        if not f:
            print("debug - not using files?")
            if not q:
                print("debug - not using form or args either?")
                return "Didn't send files", 400
        else:
            print("debug - you sent something")
            # check for a part named 'file'
            # make sure the filename is safe with secure_filename
            # fixme: secure_filename only accept ascii - can I make it accept CJK characters too? probably shouldn't...
            # sec_filename can return empty string, in which case it is caller's responsiilty to abort, or else choose its own file name
            response = make_response()
            response.headers["Location"] = dummy_path
            return response, 201
    else:
        return "I'm a media endpoint", 200

@app.route("/masters/", defaults={"name" : ""})
@app.route("/masters/<name>")
@login_required
def a_masters(name):
    if not name:
        return redirect(url_for("masters"))
    else:
        filepath = os.path.join(MASTERS_FOLDER, name)
        page = read_post(filepath)
        return render_template("base.html", post=page)

@app.route("/masters")
@login_required
def masters():
    filepath = os.path.join(MASTERS_FOLDER, "index")
    indexpage = read_post(filepath)
    return render_template("base.html", post=indexpage)


@app.route("/api/v1/test/goblin")
def test_goblin():
    return '''<!doctype html>
    <html>
    <head>
    <title>Goblin Time</title>
    <meta charset="utf-8" />
    <script type="module" src="/static/goblin.js" defer></script>
    </head>
    <body>
    <main>
    <h1>Goblin Time</h1>
    <?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg class="gob" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="0 0 640 640" width="640" height="640"><defs><path d="M470.58 261.43C468.4 257.77 471.32 262.67 469.14 259.01C463.93 250.24 452.58 247.36 443.81 252.58C422.27 265.39 373.53 294.39 351.98 307.21C343.21 312.43 340.34 323.76 345.55 332.53C347.73 336.2 344.82 331.3 346.99 334.96C352.21 343.73 363.55 346.61 372.32 341.39C393.86 328.58 442.61 299.58 464.15 286.76C472.92 281.54 475.8 270.2 470.58 261.43Z" id="a2K0Htlck" class="a1"></path><path d="M470.58 261.43C468.4 257.77 471.32 262.67 469.14 259.01C463.93 250.24 452.58 247.36 443.81 252.58C422.27 265.39 373.53 294.39 351.98 307.21C343.21 312.43 340.34 323.76 345.55 332.53C347.73 336.2 344.82 331.3 346.99 334.96C352.21 343.73 363.55 346.61 372.32 341.39C393.86 328.58 442.61 299.58 464.15 286.76C472.92 281.54 475.8 270.2 470.58 261.43Z" id="b7iHdv7VBG" class="a1"></path><path d="M349.46 484.85L301.61 455.51L334.42 430.78L367.23 406.05L382.26 460.12L397.3 514.19L349.46 484.85Z" id="b183tuBUBZ"></path><path d="M240.54 500.07L220 447.83L261.08 447.83L302.17 447.83L281.63 500.07L261.08 552.3L240.54 500.07Z" id="b16wA0WMsk"></path><path d="M380.87 379.71C380.87 430.7 337.94 472.04 284.98 472.04C232.02 472.04 189.09 430.7 189.09 379.71C189.09 328.71 232.02 287.38 284.98 209.62C337.94 287.38 380.87 328.71 380.87 379.71Z" id="b4u6h5cz0P"></path><path d="M226.19 340.37C224.01 344.03 226.93 339.13 224.75 342.8C219.53 351.57 208.19 354.44 199.42 349.22C177.88 336.41 129.13 307.41 107.59 294.6C98.82 289.38 95.94 278.04 101.15 269.27C103.33 265.6 100.42 270.51 102.6 266.85C107.82 258.08 119.16 255.19 127.92 260.41C149.47 273.23 198.21 302.23 219.75 315.04C228.52 320.26 231.41 331.6 226.19 340.37Z" class="a2" id="a96x7nMsY"></path><path d="M226.19 340.37C224.01 344.03 226.93 339.13 224.75 342.8C219.53 351.57 208.19 354.44 199.42 349.22C177.88 336.41 129.13 307.41 107.59 294.6C98.82 289.38 95.94 278.04 101.15 269.27C103.33 265.6 100.42 270.51 102.6 266.85C107.82 258.08 119.16 255.19 127.92 260.41C149.47 273.23 198.21 302.23 219.75 315.04C228.52 320.26 231.41 331.6 226.19 340.37Z" id="aaCuDW3qt" class="a2"></path><path d="M284.98 269.4C250.32 269.4 222.18 241.26 222.18 206.6C222.18 171.93 250.32 143.8 284.98 143.8C319.65 143.8 347.78 171.93 347.78 206.6C347.78 241.26 319.65 269.4 284.98 269.4Z" id="a2Huhp9U7p"></path><path d="M284.98 269.4C250.32 269.4 222.18 241.26 222.18 206.6C222.18 171.93 250.32 143.8 284.98 143.8C319.65 143.8 347.78 171.93 347.78 206.6C347.78 241.26 319.65 269.4 284.98 269.4Z" id="cp7wCbwnU"></path><path d="M257.54 161.77L217.91 119.23L211.14 128.75L182.46 98.1L210.48 156.95L220.16 143.36L246.9 176.72L257.54 161.77Z" id="aDyYt3EkM"></path><path d="M315.34 162.85L354.97 120.31L361.74 129.83L390.42 99.18L362.39 158.03L352.72 144.44L325.98 177.8L315.34 162.85Z" id="egF5gG2eV"></path><path d="M312.15 190.39C312.15 195.38 308.1 199.43 303.12 199.43C298.13 199.43 294.08 195.38 294.08 190.39C294.08 185.41 298.13 181.36 303.12 181.36C308.1 181.36 312.15 185.41 312.15 190.39Z" id="h9ndaAzfsA"></path><path d="M276.93 190.39C276.93 195.38 272.88 199.43 267.89 199.43C262.91 199.43 258.86 195.38 258.86 190.39C258.86 185.41 262.91 181.36 267.89 181.36C272.88 181.36 276.93 185.41 276.93 190.39Z" id="d4rWpngPXO"></path></defs><g><g><g><use xlink:href="#a2K0Htlck" opacity="1" fill="#000000" fill-opacity="0"></use><g><use xlink:href="#a2K0Htlck" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#b7iHdv7VBG" opacity="1" fill="#6ec536" fill-opacity="1"></use><g><use xlink:href="#b7iHdv7VBG" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="0"></use></g></g><g><use xlink:href="#b183tuBUBZ" opacity="1" fill="#6ec536" fill-opacity="1"></use><g><use xlink:href="#b183tuBUBZ" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#b16wA0WMsk" opacity="1" fill="#6ec536" fill-opacity="1"></use><g><use xlink:href="#b16wA0WMsk" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#b4u6h5cz0P" opacity="1" fill="#6ec536" fill-opacity="1"></use><g><use xlink:href="#b4u6h5cz0P" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#a96x7nMsY" opacity="1" fill="#6ec536" fill-opacity="1"></use><g><use xlink:href="#a96x7nMsY" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="0"></use></g></g><g><use xlink:href="#aaCuDW3qt" opacity="1" fill="#000000" fill-opacity="0"></use><g><use xlink:href="#aaCuDW3qt" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#a2Huhp9U7p" opacity="1" fill="#6ec536" fill-opacity="1"></use><g><use xlink:href="#a2Huhp9U7p" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="0"></use></g></g><g><use xlink:href="#cp7wCbwnU" opacity="1" fill="#000000" fill-opacity="0"></use><g><use xlink:href="#cp7wCbwnU" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#aDyYt3EkM" opacity="1" fill="#ecd897" fill-opacity="1"></use><g><use xlink:href="#aDyYt3EkM" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#egF5gG2eV" opacity="1" fill="#ecd897" fill-opacity="1"></use><g><use xlink:href="#egF5gG2eV" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#h9ndaAzfsA" opacity="1" fill="#ee8444" fill-opacity="1"></use><g><use xlink:href="#h9ndaAzfsA" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#d4rWpngPXO" opacity="1" fill="#ee8444" fill-opacity="1"></use><g><use xlink:href="#d4rWpngPXO" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g></g></g></svg>
    </main>
    </body>
    </html>''', 200


# <object class="gob" type="image/svg+xml" data="/static/goblin.svg"></object>
