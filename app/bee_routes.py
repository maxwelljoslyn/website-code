from app import app
from flask import request, jsonify
from .bee_crud import create_chinup, read_chinup, render_chinup_datapoint, do_it
import sqlite3, pytz
from urllib.parse import urlparse
from pathlib import Path
import requests as r
import os



@app.route("/api/v1/beeminder/chinup", methods=["GET","POST"])
def chinup():
    if request.method == "GET":
        html = do_it()
        return "".join(html), 200
    else:
        # POST
        # fixme: to protect against malicious payloads, validate request by querying Beeminder for the datapoint ID
        datapoint = None
        if request.json:
            datapoint = request.get_json()
        else:
            datapoint = request.values
        if datapoint == None:
            return "Bad request", 400
        else:
            if datapoint.get('action') != 'ADD':
                return "Not supported", 501
            else:
                try:
                    create_chinup(datapoint)
                    return "Chinup created!", 200
                except sqlite3.Error as e:
                    print(e)
                    return "Couldn't create chinup", 500

"""https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#webhook-endpoint-tips

'Your endpoint should send its HTTP response as fast as possible. If you wait too long, GitLab may decide the hook failed and retry it.'

Therefore: save the JSON, then immediately return 200 to Gitlab. After that, can take sweet time communicating with Beeminder.
"""


"""
https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#push-events

'... despite only 20 commits being present in the commits attribute, the total_commits_count attribute will contain the actual total.'
"""

base_url = "https://www.beeminder.com/api/v1/"

def upload_beeminder(auth_token, goal, value, comment=None, user=app.config["BEEMINDER_USER"]):
    endpoint = f"users/{user}/goals/{goal}/datapoints.json"
    params = dict(auth_token=auth_token,
                  value=value)
    if comment:
        params["comment"] = comment
    try:
        response = r.post(base_url + endpoint,
                         params,
                         timeout=1,)
        return response
    except Exception as e:
        raise e

def handle_gitlab(payload):
    # fixme: if additional post-gitlab-push actions become relevant, break into own functions, then have handle_gitlab call them all
    auth_token = app.config["BEEMINDER_AUTH"]
    num_commits = payload["total_commits_count"]
    project_name = payload["project"]["name"].lower()
    try:
        response = upload_beeminder(auth_token,
                                    "coding",
                                    num_commits,
                                    comment=project_name)
        print(response.text)
    except Exception as e:
        print("Beeminder upload failed:", e)


       
@app.route("/api/v1/gitlab", methods=["GET","POST"])
def gitlab():
    if request.method == "GET":
        return "Yo", 200
    else:
        # POST
        payload = request.get_json()
        app.task_queue.enqueue("app.bee_routes.handle_gitlab", payload)
        return ("", 204) # No Content - Gitlab ignores return code
        # fixme: validate request against secret header
