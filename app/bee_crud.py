from app import app
import datetime as dt
import sqlite3, pytz


US_PACIFIC = pytz.timezone('US/Pacific')
# WEBHOOK datapoint looks like this

# {
#  'urtext': '9 1 "test"',
#  'origin': 'Beeminder Web',
#  'daystamp': '20201109',
#  'created': '1604984406',
#  'value': '1.0',
#  'comment': 'test',
#  'id': '5faa1e5655c1335ec80007b8'
# }

# API datapoint DOES NOT HAVE THE SAME FIELDS

  # {
  #   "timestamp": 1604201958,
  #   "value": 5.0,
  #   "comment": "",
  #   "id": "5f9e2de955c1337e86000bec",
  #   "updated_at": 1604201961,
  #   "requestid": "1604201961a88thichehrosuz66",
  #   "canonical": "31 5",
  #   "fulltext": "2020-Oct-31 entered at 20:39 via Beedroid",
  #   "origin": "Beedroid",
  #   "daystamp": "20201031"
  # }

# in API, these are the same as in Webhook:
# daystamp, origin, value, comment, id
# WH "urtext" and API "canonical" appear to be same
# WH "created" and API "timestamp" appear to be same

def coerce_api_to_webhook(data):
    # careful not to mutate data in place; dicts, being mutable, are passed by reference
    result = {}
    result.update(data)
    if "canonical" in data:
        result["urtext"] = data["canonical"]
        del result["canonical"]
    if "timestamp" in result:
        result["created"] = data["timestamp"]
        del result["timestamp"]
    return result


def create_chinup(data):
    conn = sqlite3.connect(app.config["BEEMINDER_DATABASE"])
    try:
        c = conn.cursor()
        bongoman = {k:data[k] for k in ("urtext",
                                        "origin",
                                        "daystamp",
                                        "created",
                                        "value",
                                        "comment")}
        bongoman.update({"bee_id" : data["id"]})
        c.execute('''INSERT INTO chinups (urtext, origin, daystamp, created, value, comment, bee_id)  VALUES (:urtext, :origin, :daystamp, :created, :value, :comment, :bee_id)''', bongoman)
        conn.commit()
        c.close()
        conn.close()
    except sqlite3.Error as e:
        print("error: ", e)
        raise e

def read_chinup():
    conn = sqlite3.connect(app.config["BEEMINDER_DATABASE"])
    # https://docs.python.org/3.8/library/sqlite3.html#sqlite3.Row
    conn.row_factory = sqlite3.Row
    try:
        c = conn.cursor()
        c.execute("SELECT created, value FROM chinups ORDER BY created DESC")
        data = c.fetchall()
        c.close()
        conn.close()
        return data
    except sqlite3.Error as e:
        print("error: ", e)
        raise e



def iso_day(a_datetime):
    return f"{a_datetime.date().isoformat()}"

def render_chinup_datapoint(created_iso, dp):
    return f"On {created_iso.date().isoformat()}, at {created_iso.time().isoformat()}, there were {dp['value']}"

# def aggregate(ps):
#     """Calculate daily totals of Beeminder data point values."""
#     result = {}
#     for p in ps:
#         day = iso_day(p['daystamp'])
#         if day not in result:
#             result[day] = p['value']
#         else:
#             result[day] += p['value']
#     return result

def time_left_until_midnight():
    now = dt.datetime.now(tz=US_PACIFIC)
    midnight = dt.datetime.combine(now, dt.time(23, 59, 59, tzinfo=US_PACIFIC))
    return now, midnight - now


def make_an_iso(dp):
    return dt.datetime.fromtimestamp(dp["created"], tz=US_PACIFIC)

def do_it():
    datapoints = read_chinup()
    html = ["<p>You got it.</p>"]
    html.append("<ol>")
    setmax = 0
    daymax = 0
    totals = {}
    # fixme: figure out how to eliminate this extra loop by mungin the rows as they return from db
    size = len(datapoints)
    datapoints = datapoints[::-1]
    for idx, dp in enumerate(datapoints):
        html.append("<li>")
        created_iso = make_an_iso(dp)
        day = iso_day(created_iso)
        datum = dp["value"]
        if day not in totals:
            totals[day] = datum
        else:
            totals[day] += datum
        q = render_chinup_datapoint(created_iso, dp)
        if datum > setmax:
            setmax = datum
            html.append(q + " - new setmax!")
        else:
            html.append(q)
        # determine whether daily total should be rendered
        day_finished = False
        try:
            next_day = iso_day(make_an_iso(datapoints[idx+1]))
            day_finished = (day != next_day)
        except IndexError:
            # final day in list
            day_finished = True
        if day_finished:
            # show daily total
            r = f"; total: {totals[day]}"
            if totals[day] > daymax:
                daymax = totals[day]
                html.append(r + " - new daymax!")
            else:
                html.append(r)
        html.append("</li>")
    html.append("</ol>")
    now, remaining = time_left_until_midnight()
    html.append(f"<p>setmax: {setmax}; daymax: {daymax}</p>")
    html.append(f"<p>{remaining} remaining to set new daymax!</p>")
    hours_remaining = remaining.total_seconds() / 3600.0
    x = f"{now.year}-{now.month:02}-{now.day:02}"
    chinups_done_today = totals.get(x, 0)
    amount_to_improve_daymax = daymax - chinups_done_today + 1
    hourly = amount_to_improve_daymax / hours_remaining
    html.append(f"""<p>Today you've done {chinups_done_today}.
Do just {amount_to_improve_daymax} more to set a new daymax!
That's only {round(hourly, 1)}/hour!</p>""")
    return html
